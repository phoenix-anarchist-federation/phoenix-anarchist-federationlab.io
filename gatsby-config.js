module.exports = {
  siteMetadata: {
    siteUrl: "https://phoenix-anarchist-federation.gitlab.io",
    title: "Phoenix Anarchist Federation",
  },
  plugins: [
    "gatsby-plugin-sass",
    "gatsby-plugin-image",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    "gatsby-transformer-remark",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    {
      resolve: "gatsby-plugin-seo",
      options: {
        siteName: "Phoenix Anarchist Federation",
        defaultSiteImage: "/logo.png",
        siteUrl: "https://phoenix-anarchist-federation.gitlab.io",
        twitterCreator: "@PhxAnarFed",
        twitterSite: "@PhxAnarFed",
        globalSchema: `{
            "@type": "WebSite",
            "@id": "https://phoenix-anarchist-federation.gitlab.io#website",
            "url": "https://phoenix-anarchist-federation.gitlab.io",
            "name": "Phoenix Anarchist Federation",
            "publisher": {
              "@id": "https://phoenix-anarchist-federation.gitlab.io/about/#organization"
            },
            "image": {
              "@type": "ImageObject",
              "@id": "https://phoenix-anarchist-federation.gitlab.io#logo",
              "url": "https://phoenix-anarchist-federation.gitlab.io/logo.png",
              "caption": "P.A.F. Logo"
            }
          }`
      }
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: " ",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: "./src/pages/",
      },
      __key: "pages",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "posts",
        path: "./posts/",
      },
      __key: "posts",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "platform",
        path: "./platform/",
      },
      __key: "platform",
    },
  ],
};
