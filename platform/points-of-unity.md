---
title: Our Points of Unity
order: 0
---

## Anarchism
We identify ourselves as anarchists of the social, anarcho-communist, and platformist traditions. We broadly identify with the theoretical base of this tradition and the organizational practice it argues for, but not everything else it has said or done. It is simply a starting point for our politics and not necessarily an end. Therefore, we hold the core of this tradition and the developmental goal of the anarchist organization to be Theoretical Unity, Tactical Unity, Collective Action and Discipline, and Federalism.

## Power
We reject the present social system of state and capitalist centralization, as it is founded on the principle of the State which is contrary to the initiative and freedom of the people. Every form of power involves economic, political, spiritual, racial, and gender privilege. Its application on an economic level is represented by private property and the impoverishment of the oppressed classes, on a political level by the state, on a spiritual level by religion, on a racial level by white supremacy and colonialism, and on a gender level by patriarchy and queer oppression. These 5 forms of power are linked. If you touch one, the others are changed, and inversely, if you keep one form of power, it will inevitably lead to the re-establishment of the others. We seek the destitution of power and the abolition of all hierarchical institutions and social relationships. 

## Private Property
We believe private property is a tool of oppression by the bourgeoisie on the oppressed classes and the cause of its woes. Control of the means of life must be in the hands of all, not the wealthy few. We believe in the dissolution of private property and the establishment of collective ownership of the land and means of life to be worked by all and for all.

## The State
The state is the guard dog of capital and minority class rule. It is a tool of despotism, privilege, and class segregation no matter who claims its mantle. To bring about a free society it must be abolished. 

## Revolution
Capitalism, The State, and other forms of oppressive institutions cannot be abolished through legal evolution, nor bourgeoisie democracy. Rather the oppressed classes must engage in a revolutionary struggle to abolish the state, expropriate the land and the means of life, and establish a society free of class and all forms of oppression.

# Internationalism
Capitalism as a global system cannot be opposed on a national basis. Rather we call for an international revolutionary movement unbound by national borders to challenge and destroy the global capitalist system. 

# White Supremacy
The settler state of the United States rests on stolen indigenous land and is fundamentally founded upon and perpetuates white supremacy. All forms of racial domination whether that be institutional or within social relations must be opposed.

# The Labor Movement
We as anarchists must support and join the oppressed classes struggle to achieve ever greater concessions from the capitalist classes. We must prevent the control of the movement by political parties and ensure the unions aren’t dominated by elite functionaries but rather are instead administered by alternating volunteers. Anarchists must ensure the spread of anarchist propaganda within these worker organizations to bring others to our cause in the emancipation of mankind from the capitalist system. We do not however believe we should dominate these worker organizations nor that they should be anarchist, rather they should be neutral to ensure all workers are welcome. 

# The Struggle of the Oppressed
We see anarchist participation in the struggle of all oppressed classes outside of the workplace as vital. Anarchists should be encouraged to enter local groups and movements against oppression; to both support their cause and spread anarchist ideas and methods of organization.

# The Environment
The social revolution must work to stem the tide of ecological collapse and reach a balance with nature rather than the endless exploitation fostered by capitalism. Not only for the survival of humanity but also to ensure all forms of life. This is not a rejection of technology but a rejection of the industrial development that has ravaged the world around us and brought upon ecological collapse.