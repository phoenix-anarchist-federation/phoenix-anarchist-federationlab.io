---
title: Our Tactics
order: 1
---
The  anarchist social revolution can only be achieved through the united  action of the oppressed classes inspired by anarchism and organized into  nonhierarchical libertarian institutions. Therefore, we task our fellow  anarchists to support the spread of anarchist ideas in these  institutions and build them where they do not exist leaving them open to  all members of the oppressed classes. 

We  repudiate any attempt at state power as the state will always lead to  class segregation and violent oppression no matter who claims its  mantle. Therefore, we oppose those who seek state power under the guise  of a “workers” party as no free society can be created through the tools  of the state making them inherently ineffective.  

We  proclaim direct action as the only efficient tool of the oppressed in  their struggle that can act in defense of immediate material interests  and the in creation of a free society through the social revolution. 

Our  immediate goals are to build greater contact and organize with our  fellow anarchists, to make allies of local groups who share our means,  to spread and develop anarchist infrastructure, to build anarchist  educational structures within the organization and to spread anarchist  ideas both within and without. 