---
title: 'Collectives - Their Organization and Function'
date: September 23, 2021
---
## What is a Collective?

Phoenix Anarchist Federation (P.A.F.) is made up of autonomous anarchist collectives united under the program and banner of the federation. Each collective is based on geographic location and is made up of at least two or more members from that locality. Collectives are completely autonomous within their area, free to pursue activities as they see fit and work with other collectives from different localities if they don’t violate the points of unity of the P.A.F.

## The Organization of the Collective? 

Collectives are organized along nonhierarchic lines and rely on consensus and in some cases voting to decide on actions and decisions within the greater federation. Collectives when interacting with the greater federation appoint a delegate to represent them. Delegates have no decision-making ability themselves and must follow the consensus of their collective on all matters. Collectives may form committees to tackle any manner of projects and problems, but these have no decision-making abilities without the consensus of the collective. 

## The Function of the Collective? 

The overall function of the collective is the tackling of local issues as anarchists. Collectives are fundamentally ideologically based organizations that must work to build solidarity within their community as well as spread anarchist ideas and projects. Collectives should seek to enter local groups and movements even if not outright anarchist to both support their cause and spread anarchist ideas. Anarchist Collectives must pursue actual material improvements for all those oppressed while ensuring they never fall into reformism and forget our goal of the social revolution and the destruction of all oppressive institutions.

