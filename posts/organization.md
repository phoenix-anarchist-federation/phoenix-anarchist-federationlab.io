---
title: 'The Organizational Platform of the Phoenix Anarchist Federation'
date: October 16, 2021
---
## Our Points of Unity

1. We identify ourselves as anarchists of the social, anarcho-communist, and platformist traditions. We broadly identify with the theoretical base of this tradition and the organizational practice it argues for, but not everything else it has said or done. It is simply a starting point for our politics and not necessarily an end. Therefore, we hold the core of this tradition and the developmental goal of the anarchist organization to be Theoretical Unity, Tactical Unity, Collective Action and Discipline, and Federalism.

2. We reject the present social system of state and capitalist centralization, as it is founded on the principle of the State which is contrary to the initiative and freedom of the people. Every form of power involves economic, political, spiritual, racial, and gender privilege. Its application on an economic level is represented by private property and the impoverishment of the oppressed classes, on a political level by the state, on a spiritual level by religion, on a racial level by white supremacy and colonialism, and on a gender level by patriarchy and queer oppression. These 5 forms of power are linked. If you touch one, the others are changed, and inversely, if you keep one form of power, it will inevitably lead to the re-establishment of the others. We seek the destitution of power and the abolition of all hierarchical institutions and social relationships. 

3. Private property is a tool of oppression by the bourgeoisie on the oppressed classes and the cause of its woes. Control of the means of life must be in the hands of all, not the wealthy few. We believe in the dissolution of private property and the establishment of collective ownership of the land and means of life to be worked by all and for all.

4. The state is the guard dog of capital and minority class rule. It is a tool of despotism, privilege, and class segregation no matter who claims its mantle. To bring about a free society it must be abolished. 

5. Capitalism, The State, and other forms of oppressive institutions cannot be abolished through legal evolution, nor bourgeoisie democracy. Rather the oppressed classes must engage in a revolutionary struggle to abolish the state, expropriate the land and the means of life, and establish a society free of class and all forms of oppression.

6. Capitalism as a global system cannot be opposed on a national basis. Rather we call for an international revolutionary movement unbound by national borders to challenge and destroy the global capitalist system. 

7. The settler state of the United States rests on stolen indigenous land and is fundamentally founded upon and perpetuates white supremacy. All forms of racial domination whether that be institutional or within social relations must be opposed.

8. We must support and join the workers' struggle to achieve ever greater concessions from the capitalist classes. We must prevent the control of the workers' movement by political parties and ensure the unions aren’t dominated by elite functionaries but are instead administered by alternating volunteers. Anarchists must ensure the spread of anarchist propaganda within these worker organizations to bring others to our cause in the emancipation of mankind from the capitalist system. We do not believe we should dominate these worker organizations nor that they should be anarchist, rather they should be neutral to ensure all workers are welcome. 

9. We see anarchist participation in the struggle of all oppressed classes outside of the workplace as vital. Anarchists should be encouraged to enter local groups and movements against oppression; to both support their cause and spread anarchist ideas and methods of organization.

10. The social revolution must work to stem the tide of ecological collapse and reach a balance with nature rather than the endless exploitation fostered by capitalism. Not only for the survival of humanity but also to ensure all forms of life. This is not a rejection of technology but a rejection of the industrial development that has ravaged the world around us and brought upon ecological collapse. 

Tactics

The anarchist social revolution can only be achieved through the united action of the oppressed classes inspired by anarchism and organized into nonhierarchical libertarian institutions. Therefore, we task our fellow anarchists to support the spread of anarchist ideas in these institutions and build them where they do not exist leaving them open to all members of the oppressed classes. 

We repudiate any attempt at state power as the state will always lead to class segregation and violent oppression no matter who claims its mantle. Therefore, we oppose those who seek state power under the guise of a “workers” party as no free society can be created through the tools of the state making them inherently ineffective.  

We proclaim direct action as the only efficient tool of the oppressed in their struggle that can act in defense of immediate material interests and the in creation of a free society through the social revolution. 

Our immediate goals are to build greater contact and organize with our fellow anarchists, to make allies of local groups who share our means, to spread and develop anarchist infrastructure, to build anarchist educational structures within the organization and to spread anarchist ideas both within and without. 

Organization

Phoenix Anarchist Federation (P.A.F.) is made up of autonomous anarchist collectives united under the program and banner of the federation. Each collective is based on geographic location and is made up of at least two or more members from that locality. Collectives are completely autonomous within their area, free to pursue activities as they see fit and work with other collectives from different localities if they don’t violate the points of unity of the P.A.F.

Collectives are organized along nonhierarchic lines and rely on consensus and in some cases voting to decide on actions and decisions within the greater federation. Collectives when interacting with the greater federation appoint a delegate to represent them. Delegates have no decision-making ability themselves and must follow the consensus of their collective on all matters. Collectives may form committees to tackle any manner of projects and problems, but these have no decision-making abilities without the consensus of the collective. Collectives and the committees they form must ensure they are maintaining tactical unity and not violating P.A.F. points of unity. 

The overall function of the collective is the tackling of local issues as anarchists. Collectives are fundamentally ideologically based organizations that must work to build solidarity within their community as well as spread anarchist ideas and projects. Collectives should seek to enter local groups and movements even if not outright anarchist to both support their cause and spread anarchist ideas. Anarchist Collectives must pursue actual material improvements for all those oppressed while ensuring they never fall into reformism and forget our goal of the social revolution and the destruction of all oppressive institutions.

We see the education and development of Phoenix Anarchist Federation members as a priority to ensure members fully understand the points of unity and can further the development of anarchist ideas and institutions within their communities. Therefore, education of its members is both the task of the federation and the collectives. Each collective must ensure every member has a fundamental understanding of anarchism and have the structures in place for the education and development of its members. The Federation as a whole must layout an educational program for its members and collectives to follow and develop. 