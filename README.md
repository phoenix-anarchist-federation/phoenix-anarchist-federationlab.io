<div align="center">
  <img src="./src/images/phx-logo.png" width="100" alt="PAF logo">
  <br />
  <h1>Phoenix Anarchist Federation</h1>
</div>

> Website of the Phoenix Anarchist Federation

## Who We Are
We are a Federation of Anarchist collectives dedicated to spreading anarchist ideas and building anarchist infrastructure in Phoenix.

## License

[The Anti-Capitalist Software License](https://anticapitalist.software/)
