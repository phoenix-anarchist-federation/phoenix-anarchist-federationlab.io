import React from 'react';

const AboutUs = () => (
  <section className="section">
    <h1 className="title has-text-centered">About Us</h1>
    <h2 className="subtitle has-text-centered">
      Who we are
    </h2>
    <div className="content">
      <div className="container has-text-centered mt-1 mb-6">
        <p>We are a Federation of Anarchist collectives dedicated to spreading anarchist ideas and building anarchist infrastructure in Phoenix.</p>
      </div>
      <div className="columns">
        <div className="column is-half">
          <h3>An Anarchist Specific Group</h3>
          <p>While we have no qualms working with others to pursue common goals we believe in the creation of anarchist exclusive groups to pursue collective projects that advance anarchism and are not diluted by other ideologies.</p>
        </div>
        <div className="column is-half">
          <h3>Direction Action and Propaganda</h3>
          <p>We believe that direct action must be complemented with propaganda to achieve any sort of material gains. We strive to build up the ability to not only help those in our community but to spread anarchist ideas and politics alongside these projects. </p>
        </div>
      </div>
      <div className="columns">
        <div className="column is-half">
          <h3>Development of Anarchist Spaces</h3>
          <p>We see the need for anarchist spaces within our communities to not only give members of our movement a place to gather and organize but also create a permanent anarchist presence within our communities. </p>
        </div>
        <div className="column">
          <h3>Anarchist Education</h3>
          <p>The educational development of our members is a key part of our organization. To both ensure they understand fully our points of unity and program but can also effectively spread, defend and develop anarchist ideas.</p>
        </div>
      </div>
    </div>
  </section>
);

export default AboutUs;
