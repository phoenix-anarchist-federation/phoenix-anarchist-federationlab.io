import React from 'react';
import Header from './Header';

const Hero = () => (
  <div className="hero is-large is-primary has-navbar-fixed-top">
    <div className="hero-head">
      <Header />
    </div>
    <div className="hero-body">
      <div className="container has-text-centered">
        <p className="title is-1">
          Welcome To The Phoenix Anarchist Federation
        </p>
      </div>
    </div>
  </div>
);

export default Hero;
