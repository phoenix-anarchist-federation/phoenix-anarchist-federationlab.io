import React from "react";
import '../styles.scss';

const Layout = ({ children }) => {
  return (
    <React.Fragment>
      {children}
    </React.Fragment>
  );
};

export default Layout;