import React from "react";
import { StaticImage } from "gatsby-plugin-image";

const Posts = ({ posts }) => (
  <div className="container">
    {posts.map((post) => (
      <article className="media">
        <figure className="media-left">
          <p className="image is-64x64">
            <StaticImage
              src="../images/paf-logo.png"
              alt="A dinosaur"
            />
          </p>
        </figure>
        <div className="media-content">
          <div className="content">
            <p>{post.date}</p>
            <h4 className="title">{post.title}</h4>
            <p>{post.excerpt}</p>
          </div>
        </div>
        <nav className="level is-mobile">
          <div className="level-left">
            <a href="" className="level-item">
              <span className="icon is-small">
                <i className="fas fa-reply" />
              </span>
            </a>
            </div>
        </nav>
      </article>
    ))}
  </div>
);

export default Posts;
