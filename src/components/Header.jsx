import React from 'react';
import { StaticImage } from "gatsby-plugin-image";

const Header = () => {
  return (
    <nav className="navbar is-fixed-top" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <a href="" className="navbrand-item">
          <StaticImage
            src="../images/paf-logo.png"
            alt="A dinosaur"
          />
        </a>
        <a role="button" aria-label="menu" className="navbar-burger" aria-expanded="false" data-target="navbarBasicExample">
          <span aria-hidden="true" />
          <span aria-hidden="true" />
          <span aria-hidden="true" />
        </a>
      </div>
      <div id="navbarBasicExample" className="navbar-menu">
        <div className="navbar-end">
          <a href="/" className="navbar-item">Home</a>
          <a href="/platform" className="navbar-item">PAF Platform</a>
          <a href="/" className="navbar-item">Projects Us</a>
        </div>
      </div>
    </nav>  
  );
};

export default Header;
