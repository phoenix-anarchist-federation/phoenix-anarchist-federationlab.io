import { graphql } from "gatsby";
import React from "react"
import { SEO, useSEO } from "gatsby-plugin-seo";
import Hero from "../components/Hero";
import Layout from "../components/Layout";
import Posts from '../components/Posts';
import AboutUs from "../sections/AboutUs";
import NewToAnarchism from "../sections/NewToAnarchism";

const IndexPage = ({ data }) => {
  const { siteUrl } = useSEO();

  const posts = data.posts.nodes.map((post) => ({
    title: post.frontmatter.title,
    date: post.frontmatter.date,
    excerpt: post.excerpt,
  }));

  return (
    <Layout>
      <SEO
        title="Welcome"
        description="Welcome To The Phoenix Anarchist Federation"
        pagePath="/"
        schema={`{
          "@context": "http://schema.org",
          "@type":   "WebPage",
          "mainEntity": {
            "@type": "Organization",
            "name": "Phoenix Anarchist Federation",
            "image": "${siteUrl}/logo.png"
          }
        }`}
      />
      <Hero />
      <AboutUs />
      <NewToAnarchism />
      <Posts posts={posts} />
    </Layout>
  )
}

export const query = graphql`
  query IndexPageQuery {
    posts: allMarkdownRemark {
      nodes {
        fileAbsolutePath
        excerpt
        frontmatter {
          date
          title
        }
      }
    }
  }
`;

export default IndexPage
