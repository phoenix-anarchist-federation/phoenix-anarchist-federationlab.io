import React from "react";
import { graphql } from "gatsby";
import { SEO, useSEO } from "gatsby-plugin-seo";
import Layout from "../components/Layout";
import Header from '../components/Header';

const Platform = ({ data }) => {
  const { siteUrl } = useSEO();

  return (
    <Layout>
      <SEO
        title="PAF Platform"
        description="Platform of the Phoenix Anarchist Federation"
        pagePath="/platform"
      />
      <Header />
      <section className="section">
        {data.allFile.nodes.map((node) => (
          <section id={node.name} key={node.name} className="section">
            <div className="container">
              <h1 className="title has-text-centered">{node.markdown[0].frontmatter.title}</h1>
              <div className="content" dangerouslySetInnerHTML={{ __html: node.markdown[0].html }} />
            </div>
          </section>  
        ))}
      </section>
    </Layout>
  );
};

export const query = graphql`
  query PlatformPageQuery {
    allFile (
      filter: {sourceInstanceName: {eq: "platform"}}
      sort: {fields: childMarkdownRemark___frontmatter___order, order: ASC}
    ) {
      nodes {
        name
        markdown: childrenMarkdownRemark {
          frontmatter {
            title
          }
          html
        }
      }
    }
  }
`;

export default Platform;

